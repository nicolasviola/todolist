import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import MainScreen from './components/main-screen';
import ItemContextProvider from './hooks';
import css from './style';

const App: () => React$Node = () => {
  return (
    <ItemContextProvider>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={css.appContainer}>
        <MainScreen />
      </SafeAreaView>
    </ItemContextProvider>
  );
};

export default App;
