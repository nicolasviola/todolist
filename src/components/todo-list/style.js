import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../../generalStyle';

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.9,
    maxWidth: 400,
    paddingTop: 10,
    paddingBottom: 20,
    paddingHorizontal: 20,
    minHeight: 200,
    borderWidth: 2,
    borderRadius: 15,
    marginBottom: 20,
    borderColor: colors.lightBlue,
  },
  deleteButtonContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginBottom: 10,
  },
  deleteButton: {
    zIndex: 4,
  },
  deleteIcon: {
    width: 25,
    height: 25,
  },
  lastCard: {
    marginBottom: 80,
  },
  title: {
    fontSize: 20,
    color: colors.primary,
  },
  body: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  infoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  info: {
    fontSize: 40,
  },
  subTitle: {
    fontSize: 15,
  },
});

export default styles;
