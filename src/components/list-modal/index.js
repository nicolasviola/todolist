import React, {useState, useCallback, useContext} from 'react';
import CheckBox from '@react-native-community/checkbox';
import {
  View,
  Text,
  Modal,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  Platform,
  Image,
  FlatList,
  Keyboard,
} from 'react-native';
import css from './style';
import {ItemContext} from '../../hooks';
import closeIcon from '../../../assets/icons/close.png';
import {getCompletedCount, generateId} from '../../helpers';
import addIcon from '../../../assets/icons/add.png';
import deleteIcon from '../../../assets/icons/delete-item.png';

const ListModal = (props) => {
  const [inputValue, changeInputValue] = useState('');
  const {visible, closeModal, index} = props;
  const {updateList, todoList} = useContext(ItemContext);

  const onCheckBoxChange = useCallback(
    (todoIndex) => {
      let newlist = {...todoList[index]};
      newlist.todos[todoIndex].completed = !todoList[index].todos[todoIndex]
        .completed;
      updateList(index, newlist);
    },
    [todoList, index, updateList],
  );

  const onAddNewTask = useCallback(() => {
    let newlist = {...todoList[index]};
    newlist.todos.push({
      id: generateId(),
      title: inputValue,
      completed: false,
    });
    updateList(index, newlist);
    changeInputValue('');
    Keyboard.dismiss();
  }, [todoList, index, updateList, inputValue]);

  const onDeleteTask = useCallback(
    (todoIndex) => {
      let newlist = {...todoList[index]};
      newlist.todos.splice(todoIndex, 1);
      updateList(index, newlist);
      changeInputValue('');
      Keyboard.dismiss();
    },
    [todoList, index, updateList],
  );

  const renderTodoList = useCallback(
    (data) => {
      return (
        <View style={css.todoContainer}>
          <CheckBox
            style={css.checkbox}
            boxType={'square'}
            value={data.item.completed}
            disabled={false}
            onValueChange={() => onCheckBoxChange(data.index)}
          />
          {!data.item.completed && (
            <Text style={[css.todoText]}>{data.item.title}</Text>
          )}
          {data.item.completed && (
            <Text style={[css.todoText, css.todoTextCompleted]}>
              {data.item.title}
            </Text>
          )}

          <TouchableOpacity onPress={() => onDeleteTask(data.index)}>
            <Image style={css.deleteIcon} source={deleteIcon} />
          </TouchableOpacity>
        </View>
      );
    },
    [onCheckBoxChange, onDeleteTask],
  );

  return (
    <Modal visible={visible} animationType="slide" onRequestClose={closeModal}>
      <SafeAreaView style={css.container}>
        <KeyboardAvoidingView
          keyboardShouldPersistTaps={'handled'}
          keyboardVerticalOffset={Platform.select({
            ios: 0,
            android: -500,
          })}
          style={css.keyboard}
          behavior="padding">
          <TouchableOpacity onPress={closeModal} style={css.closeButton}>
            <Image style={css.closeIcon} source={closeIcon} />
          </TouchableOpacity>
          <View style={css.header}>
            <Text style={css.title}>{todoList[index].name}</Text>
            <Text style={css.taskCount}>
              {getCompletedCount(todoList[index])} of{' '}
              {todoList[index].todos.length} tasks
            </Text>
          </View>
          <View style={css.bodyContainer}>
            <FlatList
              data={todoList[index].todos}
              keyboardShouldPersistTaps="always"
              enableOnAndroid={true}
              contentContainerStyle={css.flatList}
              renderItem={(item) => renderTodoList(item)}
              keyExtractor={(item, todoIndex) => `${todoIndex}`}
            />
          </View>
          <View style={css.footer}>
            <TextInput
              placeholder={'Add a new task'}
              onChangeText={(text) => changeInputValue(text)}
              style={css.input}
              value={inputValue}
            />
            <TouchableOpacity
              disabled={!inputValue}
              onPress={onAddNewTask}
              style={[css.addTodoButton, !inputValue && css.addButtonDisabled]}>
              <Image style={css.addButtonIcon} source={addIcon} />
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </Modal>
  );
};

export default ListModal;
