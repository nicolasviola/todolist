# Todo List

This is a simple to-do list app example made with React Native.

## Description
This app was building using the version 0.63.2 of react-native.

## Getting started

Clone the repo:

```python
git clone https://nicolasviola@bitbucket.org/nicolasviola/todolist.git
```

Go to the project folder and install the dependencies:

```python
cd TodoList
npm install
```

## Run the project

IOS:

```python
react-native run-ios
```

Android.

```python
react-native run-android
```