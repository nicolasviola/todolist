import React, {useState, useContext} from 'react';
import {ItemContext} from '../../hooks';
import {
  View,
  Text,
  Modal,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
} from 'react-native';
import css from './style';
import closeIcon from '../../../assets/icons/close.png';
import {generateId} from '../../helpers';

const AddListModal = (props) => {
  const {visible, closeModal} = props;
  const {addNewList} = useContext(ItemContext);
  const [title, changeTitle] = useState('');

  const createButtonHandler = () => {
    if (title) {
      addNewList({
        id: generateId(),
        name: title,
        todos: [],
      });
      changeTitle('');
      closeModal();
    }
  };
  return (
    <Modal visible={visible} animationType="slide" onRequestClose={closeModal}>
      <KeyboardAvoidingView
        keyboardVerticalOffset={Platform.select({
          ios: 0,
          android: -500,
        })}
        style={css.container}
        behavior="padding">
        <TouchableOpacity onPress={closeModal} style={css.closeButton}>
          <Image style={css.closeIcon} source={closeIcon} />
        </TouchableOpacity>
        <View style={css.bodyContainer}>
          <View style={css.titleContainer}>
            <Text style={css.title}>Create Todo List</Text>
          </View>
          <TextInput
            style={css.input}
            placeholder={'List Name'}
            onChangeText={(text) => changeTitle(text)}
            value={title}
          />
          <TouchableOpacity
            style={[css.createButton, !title && css.buttonDisabled]}
            disabled={!title}
            onPress={createButtonHandler}>
            <Text style={css.createButtonText}>DONE</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default AddListModal;
