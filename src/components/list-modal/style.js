import {StyleSheet, Dimensions, Platform} from 'react-native';
import {colors} from '../../../generalStyle';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  keyboard: {
    flex: 1,
  },
  closeButton: {
    position: 'absolute',
    top: 20,
    zIndex: 2,
    right: 32,
  },
  closeIcon: {
    width: 20,
    height: 20,
  },
  header: {
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
    marginTop: 50,
    marginBottom: 10,
    borderBottomWidth: 3,
    marginHorizontal: 32,
    borderColor: colors.secondary,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: colors.primary,
    marginBottom: 5,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: colors.grey,
    fontWeight: '600',
  },
  bodyContainer: {
    flex: 3,
    width: Dimensions.get('window').width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatList: {
    width: Dimensions.get('window').width,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  footer: {
    flex: 1,
    alignSelf: 'stretch',
    paddingHorizontal: 32,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.secondary,
    flex: 1,
    maxWidth: 400,
    borderRadius: 5,
    height: 50,
    marginRight: 10,
    marginLeft: 10,
    paddingHorizontal: 8,
    fontSize: 18,
  },
  addTodoButton: {
    backgroundColor: colors.secondary,
    height: 40,
    width: 40,
    borderRadius: 30,
    padding: 10,
  },
  addButtonIcon: {
    width: 20,
    height: 20,
  },
  addButtonDisabled: {
    backgroundColor: colors.disabledGrey,
  },
  todoContainer: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    marginBottom: 20,
  },
  checkbox: {
    width: 20,
    height: 20,
    marginRight: 20,
    marginLeft: Platform.OS === 'ios' ? 0 : -10,
    alignSelf: 'center',
    borderColor: colors.grey,
  },
  deleteIcon: {
    width: 20,
    height: 20,
    marginLeft: 10,
  },
  todoText: {
    flex: 1,
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 20,
  },
  todoTextCompleted: {
    textDecorationLine: 'line-through',
    color: colors.grey,
  },
});

export default styles;
