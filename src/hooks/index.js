import React, {createContext, useState, useCallback} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export const ItemContext = createContext({});

const STORAGE_KEY = '@todo_list';

const ItemContextProvider: React.FC = ({children}) => {
  const [todoList, changeTodoList] = useState([]);

  const readTodoList = useCallback(async () => {
    try {
      const todoListOnStorage = await AsyncStorage.getItem(STORAGE_KEY);
      if (todoListOnStorage) {
        changeTodoList(JSON.parse(todoListOnStorage));
      }
    } catch (e) {
      alert('Failed to fetch the data from storage');
    }
  }, [changeTodoList]);

  const updateList = (index, newList) => {
    let newTodoList = [...todoList];
    newTodoList[index] = newList;
    changeTodoList(newTodoList);
    saveOnAsyncStorage(newTodoList);
  };

  const saveOnAsyncStorage = useCallback(async (list) => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(list));
    } catch (e) {
      alert('Failed to save the data');
    }
  }, []);

  const addNewList = useCallback(
    (newList) => {
      saveOnAsyncStorage([newList, ...todoList]);
      changeTodoList([newList, ...todoList]);
    },
    [todoList, changeTodoList, saveOnAsyncStorage],
  );

  const deleteList = useCallback(
    (index) => {
      const filterList = [...todoList];
      filterList.splice(index, 1);
      saveOnAsyncStorage(filterList);
      changeTodoList(filterList);
    },
    [todoList, changeTodoList, saveOnAsyncStorage],
  );

  return (
    <ItemContext.Provider
      value={{
        todoList,
        readTodoList,
        updateList,
        addNewList,
        deleteList,
      }}>
      {children}
    </ItemContext.Provider>
  );
};

export default ItemContextProvider;
