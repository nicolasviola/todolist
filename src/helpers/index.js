export const getCompletedCount = (list) =>
  list.todos.filter((todo) => todo.completed).length;

export const generateId = () => `${new Date().getTime()}`;
