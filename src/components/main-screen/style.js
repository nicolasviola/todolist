import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../../generalStyle';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    marginTop: 20,
    flexDirection: 'row',
  },
  divider: {
    backgroundColor: colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: 'center',
  },
  titleBlue: {
    fontWeight: '300',
    color: colors.secondary,
  },
  titleBlack: {
    fontSize: 38,
    fontWeight: 'bold',
    color: colors.primary,
    paddingHorizontal: 64,
  },
  addButtonContainer: {
    position: 'absolute',
    height: 50,
    width: 50,
    bottom: 10,
    right: 20,
    borderWidth: 2,
    borderRadius: 50,
    backgroundColor: colors.secondary,
    zIndex: 2,
    borderColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButtonIcon: {
    height: 25,
    width: 25,
  },
  listContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
    width: Dimensions.get('window').width,
  },
  emptyListContainer: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyListMessage: {
    color: colors.primary,
    fontSize: 20,
  },
});

export default styles;
