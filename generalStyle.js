export const colors = {
  primary: '#525355',
  secondary: '#24A6D9',
  white: '#FFFFFF',
  grey: '#A4A4A4',
  lightGrey: '#CACACA',
  lightBlue: '#A7CBD9',
  disabledGrey: '#C1C0C1',
};
