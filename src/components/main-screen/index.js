import React, {useState, useCallback, useContext, useEffect} from 'react';
import {View, Text, TouchableOpacity, FlatList, Image} from 'react-native';
import {ItemContext} from '../../hooks';
import css from './style';
import TodoList from '../todo-list';
import AddListModal from '../add-list-modal';
import addIcon from '../../../assets/icons/add.png';

const MainScreen = () => {
  const {readTodoList, todoList} = useContext(ItemContext);
  const [isAddTodoModalVisible, changeAddTodoModal] = useState(false);

  useEffect(() => {
    readTodoList();
  }, [readTodoList]);

  const toggleAddTodoModal = useCallback(() => {
    changeAddTodoModal(!isAddTodoModalVisible);
  }, [isAddTodoModalVisible, changeAddTodoModal]);

  return (
    <View style={css.container}>
      <AddListModal
        visible={isAddTodoModalVisible}
        closeModal={toggleAddTodoModal}
      />
      <View style={css.content}>
        <View style={css.divider} />
        <Text style={css.titleBlack}>
          Todo
          <Text style={css.titleBlue}> Lists</Text>
        </Text>
        <View style={css.divider} />
      </View>
      <View style={css.listContainer}>
        <FlatList
          data={todoList}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          keyExtractor={(item) => item.id}
          ListEmptyComponent={
            <View style={css.emptyListContainer}>
              <Text style={css.emptyListMessage}>
                There are no lists to display
              </Text>
            </View>
          }
          renderItem={({item, index}) => (
            <TodoList
              index={index}
              listData={item}
              isLastCard={index === todoList.length - 1}
            />
          )}
        />
      </View>
      <TouchableOpacity
        onPress={toggleAddTodoModal}
        style={css.addButtonContainer}>
        <Image style={css.addButtonIcon} source={addIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default MainScreen;
