import React, {useCallback, useState, useContext} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import css from './style';
import {ItemContext} from '../../hooks';
import deleteIcon from '../../../assets/icons/delete.png';
import ListModal from '../list-modal';
import {getCompletedCount} from '../../helpers';

const TodoList = (props) => {
  const {listData, isLastCard, index} = props;
  const {deleteList} = useContext(ItemContext);

  const [showList, changeShowList] = useState(false);

  const toggleListModal = useCallback(() => {
    changeShowList(!showList);
  }, [showList, changeShowList]);

  const deleteHandler = useCallback(() => deleteList(index), [
    deleteList,
    index,
  ]);

  return (
    <>
      <ListModal
        visible={showList}
        closeModal={toggleListModal}
        index={index}
      />
      <TouchableOpacity
        onPress={toggleListModal}
        style={[css.container, isLastCard && css.lastCard]}>
        <View style={css.deleteButtonContainer}>
          <TouchableOpacity style={css.deleteButton} onPress={deleteHandler}>
            <Image style={css.deleteIcon} source={deleteIcon} />
          </TouchableOpacity>
        </View>
        <Text style={css.title}>{listData.name}</Text>
        <View style={css.body}>
          <View style={css.infoContainer}>
            <Text style={css.info}>
              {listData.todos.length - getCompletedCount(listData)}
            </Text>
            <Text style={css.subTitle}>Remaining</Text>
          </View>
          <View style={css.infoContainer}>
            <Text style={css.info}>{getCompletedCount(listData)}</Text>
            <Text style={css.subTitle}>Completed</Text>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default TodoList;
